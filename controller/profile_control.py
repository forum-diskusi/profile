from models.profile import profiles

def showAllProfile():
    result = []
    for doc in profiles.objects:
        profile = {
            "namaDepan" : doc.namaDepan,
            "namaBelakang" : doc.namaBelakang,
            "domisili" : doc.domisili,
            "umur" : doc.umur,
            "pekerjaan" : doc.pekerjaan,
            "userid" : doc.userid,
        }
        result.append(profile)
    return result

def showProfile(id):
    for doc in profiles.objects(userid=id):
        profile = {
            "namaDepan" : doc.namaDepan,
            "namaBelakang" : doc.namaBelakang,
            "domisili" : doc.domisili,
            "umur" : doc.umur,
            "pekerjaan" : doc.pekerjaan,
            "userid" : doc.userid,
        }
    return profile

def insertProfile(data):
    profiles(**data).save()
    result = {"massage" : "profile berhasil ditambahkan"}
    return result

def updateProfile(id, data):
    profiles.objects(userid=id).update(**data)
    result = {"massage" : "profile berhasil diupdate"}
    return result

def deleteProfile(id):
    profiles.objects(userid=id).delete()
    result = {"massage" : "profile berhasil dihapus"}
    return result
