from mongoengine import connect
from mongoengine import Document, StringField, IntField, DynamicDocument

connection = connect(db="forumDiskusi", host="localhost", port=27017)

if connection:
    print("MongoDB Connected")

class profiles(Document):
    namaDepan = StringField(required=True, max_length=70)
    namaBelakang = StringField(required=True, max_length=70)
    domisili = StringField(required=True, max_length=70)
    umur = IntField(required=True, max_length=70)
    pekerjaan = StringField(required=True, max_length=70)
    userid = IntField(required=True, max_length=70)
