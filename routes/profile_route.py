from fastapi import APIRouter, Request
from controller.profile_control import *

route_profile = APIRouter()

@route_profile.get("/profiles")
async def showusers():
    return showAllProfile()

@route_profile.post("/profiles")
async def insertuser(request: Request):
    data = await request.json()
    return insertProfile(data)

@route_profile.get("/profile/{userid}")
async def showuserid(userid):
    return showProfile(userid)

@route_profile.put("/profile/{userid}")
async def updateuserid(userid, request: Request):
    data = await request.json()
    return updateProfile(userid, data)

@route_profile.delete("/profile/{userid}")
async def deleteuserid(userid):
    return deleteProfile(userid)
