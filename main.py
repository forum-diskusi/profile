from fastapi import FastAPI
from routes.profile_route import *

app = FastAPI()
app.include_router(route_profile)